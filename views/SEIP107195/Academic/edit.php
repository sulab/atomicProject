


<?php

        include_once ("../../../vendor/autoload.php");
        use Sulab\Bitm\SEIP107095\Academic\BookTitle;
        
        
        $book = new BookTitle ();
        $books = $book->show($_GET['id']);
        
      
        
        
?>

<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>Edit Book Title</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="../../../resource/css/bootstrap.min.css">
        <script src="../../../resource/js/jquery.min.js"></script>
        <script src="../../../resource/js/bootstrap.min.js"></script>

        
    </head>
    <body>
        <br><br>
        <div class="container">
            <form role="form" action="update.php" method="post">
            <fieldset class="form-group">
                <legend>Edit Book Title</legend>
                <input 
                           class="form-control"
                           type="hidden"
                           name="id"                         
                           value="<?php echo $books['id'];?>"
                           />  
                
                <div class="form-group">
                    <label for="title">Edit Book Title: </label>
                    <input placeholder="Enter Book Title"
                           class="form-control"
                           type="text"
                           name="title"
                           id="title"
                           autofocus="autofocus"
                           tabindex="1"
                        
                         
                           value="<?php echo $books['title'];?>"
                           />
                    
                    <label for="author">Edit Author Name: </label>
                    <input placeholder="Enter Book Title"
                           class="form-control"
                           type="text"
                           name="author"
                           id="author"
                          
                           tabindex="1"
                        
                          
                           value="<?php echo $books['author'];?>"
                           />
                </div>
                
                <div class="form-group">
                    <button tabindex="2"type="Submit" class="btn btn-info">Save</button>
                    <button tabindex="3"type="Submit" class="btn btn-info">Save & Add</button>
                    <button tabindex="4"type="reset" class="btn btn-info">Reset</button>
                
                </div>
            </fieldset>    
        </form>
        
            <nav>
                <li><a href="index.php">Go To List</a></li>
                <li><a href="javascript:history.go(-1)">Back</a></li>
            </nav>
        </div>
   </body>
</html>
