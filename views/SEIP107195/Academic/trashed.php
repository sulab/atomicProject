<?php
  include_once ("../../../vendor/autoload.php");
  
  use Sulab\Bitm\SEIP107095\Academic\BookTitle;
  use Sulab\Bitm\SEIP107095\Utility\Utility;
  
  $book = new BookTitle();
  $booktitles = $book->trashed();
  
  
?>


<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>Trashed Book Title</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
    
        <link rel="stylesheet" href="../../../resource/css/bootstrap.min.css">
        <script src="../../../resource/js/jquery.min.js"></script>
        <script src="../../../resource/js/bootstrap.min.js"></script>
        <script src="../../../resource/js/jquery-2.1.4.js"></script>
        
        
        
        <style>
            #utility{
                float: right;
                width: 34%;
                overflow: hidden;
                margin: 10px 0px;
            }
            
        </style>

    </head>
    <body>
        <div class="container"><br>
            <a href="../../../index.php"><button type="button" class="btn btn-primary">Project Homepage</button></a>   
        <h1>Book Title</h1>
        
        
        
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>Serial No.</th>
                    <th>Book Title &dArr;</th>
                    <th>Author Name</th>
                    <th>Actions</th>
                </tr>
            </thead>
            
            <tbody>
                <?php
                $sl= 1; 
                foreach ($booktitles as $booktitle){
                    
                
                ?>
                <tr>
                    <td><?php echo $sl;?></td>
                    <td><?php echo $booktitle['title'];?></td>
                    <td><?php echo $booktitle['author'];?></td>
                    <td>
                        
                        <form action="delete.php" method="post">
                            <a href="recover.php?id=<?php echo $booktitle['id'];?>" class="btn btn-info">Recover</a>

                            <input type="hidden"  name="id" value="<?php echo $booktitle['id'];?>">
                            <button type="submit" class="delete btn btn-danger">Delete</button>
                            
                        </form>
                        | Email to Friend</td>
                </tr>
                
                <?php
                $sl++;
                }
                ?>
                




                
            </tbody>
        </table>
          
        </div>
        <div><span id="utility">prev 1 | 2 | 3 next</span></div>
        <nav>
            <li style="margin-left: 100px;">
                <a href="index.php">Back</a>
            </li>
        </nav>

        <script>
             $('.delete').bind('click', function(){
                 var ditem = confirm("Are you sure you want to delete..?");
                 if(!ditem){
                    return false;
                 }
             }); 
             $('.alert').fadeOut(4150);
        </script>
        
    </body>
</html>
