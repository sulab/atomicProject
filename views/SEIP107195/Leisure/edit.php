<?php

        ini_set('display_errors','off');
        include_once ("../../../vendor/autoload.php");
        use \Sulab\Bitm\SEIP107095\Leisure\Hobby;
        
        
        
        $hobby = new Hobby();
              
        $hobbies = $hobby->show($_GET['id']);
        $hob = explode(",", $hobbies['hobby']);
        
        
?>

<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>Hobbies</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="../../../resource/css/bootstrap.min.css">
        <script src="../../../resource/js/jquery.min.js"></script>
        <script src="../../../resource/js/bootstrap.min.js"></script>

    </head>
    <body>
        <br><br>
        <div class="container">
            <form role="form" action="update.php" method="post">
            <fieldset class="form-group">
                <legend>Add Hobby</legend>
                <input 
                    class="form-control"
                    type="hidden"
                    name="id"




                    value="<?php echo $hobbies['id'];?>"
                    />  

                
                <div class="form-group">
                    <label for="username">Edit Your Name : </label>
                    <input placeholder="Enter your name "
                           class="form-control"
                           type="text"
                           name="username"
                           id="username"
                           autofocus="autofocus"
                           tabindex="1"
                           required="required"
                           size="50"
                           value="<?php echo $hobbies['username'];?>"/>
                </div>
                <b>Select Your Hobby:</b> 
                <div class="form-group checkbox">   
                    
                   
                    
                    <label>
                    <input type="checkbox" name="hobby[]" value="Reading" <?php if(in_array("Reading", $hob)) echo "checked=\"checked\""; ?>/>Reading
                    </label>
             
                
             
                    <label>
                    <input type="checkbox" name="hobby[]" value="Listening to Music" <?php if(in_array("Listening to Music", $hob)) echo "checked=\"checked\""; ?>/>Listening to Music
                    </label>
             
                
                
                    <label>
                    <input type="checkbox" name="hobby[]" value="Painting" <?php if(in_array("Painting", $hob)) echo "checked=\"checked\""; ?>/>Painting
                    </label>
             
                
              
                    <label>
                    <input type="checkbox" name="hobby[]" value="Watching Movies" <?php if(in_array("Watching Movies", $hob)) echo "checked=\"checked\""; ?>/>Watching Movies
                    </label>
            
               
                
                    <label>
                    <input type="checkbox" name="hobby[]" value="Traveling" <?php if(in_array("Traveling", $hob)) echo "checked=\"checked\""; ?>/>Traveling
                    </label>
                </div>
                
                <div class="form-group">
                    <button tabindex="3" type="Submit" class="btn btn-info">Save</button>
                    <button tabindex="4" type="Submit" class="btn btn-info">Save & Add</button>
                    <button tabindex="5" type="reset" class="btn btn-info">Reset</button>
                
                </div>
                
            </fieldset>    
        </form>
        
            <nav>
                <li><a href="index.php">Go To List</a></li>
                <li><a href="javascript:history.go(-1)">Back</a></li>
            </nav>
        </div>
   </body>
</html>
