<?php


    include_once ("../../../vendor/autoload.php");
    use \Sulab\Bitm\SEIP107095\geographicalArea\City;


    $city = new City();

    $cityname = $city->show($_REQUEST['id']);


?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>City Name</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="../../../resource/css/bootstrap.min.css">
        <script src="../../../resource/js/jquery.min.js"></script>
        <script src="../../../resource/js/bootstrap.min.js"></script>

    </head>
    
    <body>
        <br><br>
        <div class="container">
            <form role="form" action="update.php" method="post">
            <fieldset class="form-group">
                <legend>Edit City Name</legend>
                <input 
                           class="form-control"
                           type="hidden"
                           name="id"
                           
                           
                        
                         
                           value="<?php echo $cityname['id'];?>"
                           />  
                
                
                <div class="form-group">
                    <label for="username">Edit Your Name : </label>
                    <input placeholder="Enter your name "
                           class="form-control"
                           type="text"
                           name="username"
                           id="username"
                           autofocus="autofocus"
                           tabindex="1"
                           required="required"
                           value="<?php echo $cityname['username'];?>"
                           size="50"/>
                </div>
                    <div class="form-group">
                    <label for="cityname">Edit Your City : </label>
                    <select name="cityname" class="form-control">
                        <option value="Dhaka" <?php echo $cityname['cityname'] == "Dhaka" ? " selected" : ""; ?>>Dhaka</option>              
                        <option value="Rajshahi" <?php echo $cityname['cityname'] == "Rajshahi" ? " selected" : ""; ?>>Rajshahi</option>
                        <option value="Chittagong" <?php echo $cityname['cityname'] == "Chittagong" ? " selected" : ""; ?>>Chittagong</option>
                        <option value="Shylet" <?php echo $cityname['cityname'] == "Shylet" ? " selected" : ""; ?>>Shylet</option>
                        <option value="Barishal" <?php echo $cityname['cityname'] == "Barishal" ? " selected" : ""; ?>>Barishal</option>
                        <option value="Khulna" <?php echo $cityname['cityname'] == "Khulna" ? " selected" : ""; ?>>Khulna</option>
                        <option value="Rangpur" <?php echo $cityname['cityname'] == "Rangpur" ? " selected" : ""; ?>>Rangpur</option>
                        <option value="Mymensingh" <?php echo $cityname['cityname'] == "Mymensingh" ? " selected" : ""; ?>>Mymensingh</option>
                    </select>
                </div>
                
                <div class="form-group">
                    <button tabindex="2"type="Submit" class="btn btn-info">Save</button>
                    <button tabindex="3"type="Submit" class="btn btn-info">Save & Add</button>
                    <button tabindex="4"type="reset" class="btn btn-info">Reset</button>
                
                </div>
            </fieldset>    
        </form>
        
            <nav>
                <li><a href="index.php">Go To List</a></li>
                <li><a href="javascript:history.go(-1)">Back</a></li>
            </nav>
        </div>            
    </body>
</html>

