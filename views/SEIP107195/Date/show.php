<?php


    include_once ("../../../vendor/autoload.php");
    use \Sulab\Bitm\SEIP107095\Date\Birthdate;



    $birthdate = new Birthdate();

    $birthdates = $birthdate->show($_GET['id']);
?>

<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>Birthday of User</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
    
        <link rel="stylesheet" href="../../../resource/css/bootstrap.min.css">
        <script src="../../../resource/js/jquery.min.js"></script>
        <script src="../../../resource/js/bootstrap.min.js"></script>
        <script src="../../../resource/js/jquery-2.1.4.js"></script>
         <style>
            #utility{
              margin-left: 20px;
            }
            
        </style>

       
    </head>
    <body id="utility">
     

              <h1 style="text-align: center;">Your Birthday</h1>
               <dl style="text-align: center;">
                        <dt>ID</dt>
                      <dd><?php echo $birthdates['id'];?></dd>
    
                     <dt>User Name</dt>
                       <dd><?php echo $birthdates['username'];?></dd>
    
                            <dt>Birthday</dt>
                          <dd><?php echo $birthdates['birthdate'];?></dd>
                    </dl>

                    <nav>
                        <li style="text-align: center;">
                             <a href="index.php">Go To List</a>
                          </li>
                    </nav>
   </body>
</html>