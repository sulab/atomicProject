<?php
        include_once ("../../../vendor/autoload.php");
        use \Sulab\Bitm\SEIP107095\Date\Birthdate;
        
        
        
        $birthdate = new Birthdate();
        
        $birthdates = $birthdate->show($_GET['id']);
        
        ?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>Birthday</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="../../../resource/css/bootstrap.min.css">
        <script src="../../../resource/js/jquery.min.js"></script>
        <script src="../../../resource/js/bootstrap.min.js"></script>
        


        
    </head>
         <body>
                <br><br>
        <div class="container">
            <form role="form" action="update.php" method="post">
            <fieldset class="form-group">
                <legend>Edit Birthday</legend>
                
                <input 
                           class="form-control"
                           type="hidden"
                           name="id"
                           value="<?php echo $birthdates['id'];?>"
                           />  
                
                
                <div class="form-group">
                    <label for="username">Edit Your Name : </label>
                    <input placeholder="Enter your name "
                           class="form-control"
                           type="text"
                           name="username"
                           id="username"
                           autofocus="autofocus"
                           tabindex="1"
                           required="required"
                           size="50"
                           value="<?php echo $birthdates['username'];?>"
                           />
                </div>    
                    <div class="form-group">
                    <label for="birthdate">Edit Your Birthday (yyyy-mm-dd) : </label>
                    <input 
                               type="date"
                               name="birthdate"
                               id="birthdate"
                               value="<?php echo $birthdates['birthdate'];?>"
                               tabindex="2"
                               required="required"
                               class="form-control"
                               
                               />
                </div>
                
                <div class="form-group">
                    <button tabindex="3"type="Submit" class="btn btn-info">Save</button>
                    <button tabindex="4"type="Submit" class="btn btn-info">Save & Add</button>
                    <button tabindex="5"type="reset" class="btn btn-info">Reset</button>
                
                </div>
            </fieldset>    
        </form>
        
            <nav>
                <li><a href="index.php">Go To List</a></li>
                <li><a href="javascript:history.go(-1)">Back</a></li>
            </nav>
        </div>  
          </body>
</html>
