<?php
        include_once ("../../../vendor/autoload.php");
        use Sulab\Bitm\SEIP107095\Organization\Summary;
        use \Sulab\Bitm\SEIP107095\Utility\Utility;
        
        
        $summary = new Summary();
        $summaries = $summary->index();
       
       

?>

<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>Summary of Organizations</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="../../../resource/css/bootstrap.min.css">
        <script src="../../../resource/js/jquery.min.js"></script>
        <script src="../../../resource/js/bootstrap.min.js"></script>
        <script src="../../../resource/js/jquery-2.1.4.js"></script>
         <style>
            #utility{
                float: right;
                width: 30%;
            }
            #table-size{
                width: 600px;
            }
        </style>

    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-12"><br>
                    <a href="../../../index.php"><button type="button" class="btn btn-primary">Project Homepage</button></a>    
                    <h1>List of summary</h1>        
                        <div class="alert alert-success">
                            <?php echo Utility::message();?>
                        </div>
                    <div>
                        <span>Search</span> 
                        <span id="utility">Download as PDF | XL | <a href="create.php">
                                <button type="button" class="btn btn-info">Add New</button></a></span>
                    </div>
                    <br>
        
        <table class="table table-bordered table-responsive">
            <thead>
                <tr>
                    <th>Serial No.</th>
                    <th>Organization's Name</th>
                    <th>Summary </th>
                    <th>Actions</th>
                </tr>
            </thead>
            
            <tbody>
                <?php
                $sl = 1;
                foreach ($summaries as $summary){                 
                ?>
                <tr>
                    <td><?php echo $sl;?></td>
                    <td><?php echo $summary['organization'];?></td>
                    <td id="table-size"><p><?php echo $summary['summary'];?></p></td>
                    <td> 
                        <form action="delete.php" method="post">
                            <a href="edit.php?id=<?php echo $summary['id'];?>" class="btn btn-warning">Edit</a> 
                            <a href="show.php?id=<?php echo $summary['id'];?>" class="btn btn-info">View</a> 
                            <input type="hidden"  name="id" value="<?php echo $summary['id'];?>">    
                            <button type="submit" class="delete btn btn-danger">Delete</button>
                        </form> 
                           | Trash/Recover 
                           | Email to Friend
                    </td>
                </tr>
                <?php
                $sl++;
                }
                ?>   
            </tbody>
        </table>
        </div>
        </div>
        </div>
        <div><span id="utility">prev 1 | 2 | 3 next</span></div>
        <script>
             $('.delete').bind('click', function(){
                 var ditem = confirm("Are you sure you want to delete..?");
                 if(!ditem){
                    return false;
                 }
             }); 
             $('.alert').fadeOut(4150);
        </script>
        
    </body>
</html>
