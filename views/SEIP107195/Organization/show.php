<?php

    include_once ("../../../vendor/autoload.php");
    use \Sulab\Bitm\SEIP107095\Organization\Summary;



    $summary = new Summary();

    $summaries = $summary->show($_GET['id']);

    
?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>Organization's Summary</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
    
        <link rel="stylesheet" href="../../../resource/css/bootstrap.min.css">
        <script src="../../../resource/js/jquery.min.js"></script>
        <script src="../../../resource/js/bootstrap.min.js"></script>
        <script src="../../../resource/js/jquery-2.1.4.js"></script>
         <style>
            #utility{
              margin-left: 20px;
              text-align: justify;
              margin-right: 50px;
              
            }
            
        </style>

       
    </head>
    <body id="utility">
     

              <h1 style="text-align: center;">Summary of Organization</h1>
                <dl style="text-align: center;">
                    <dt>ID</dt>
                    <dd><?php echo $summaries['id'];?></dd>

                    <dt>Name of Organization</dt>
                    <dd><?php echo $summaries['organization'];?></dd>

                    <dt>Summary</dt>
                    <dd><p><?php echo $summaries['summary'];?></p></dd>
                </dl>

                    <nav>
                        <li style="text-align: center;">
                             <a href="index.php">Go To List</a>
                          </li>
                    </nav>
   </body>
</html>