<?php

    include_once ("../../../vendor/autoload.php");
    use \Sulab\Bitm\SEIP107095\Organization\Summary;



    $summary = new Summary();

    $summaries = $summary->show($_GET['id']);
    
    
?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>Summary of Organizations</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="../../../resource/css/bootstrap.min.css">
        <script src="../../../resource/js/jquery.min.js"></script>
        <script src="../../../resource/js/bootstrap.min.js"></script>

    </head>
    <body>
          <br><br>
        <div class="container">
            <form role="form" action="update.php" method="post">
            <fieldset class="form-group">
                <legend>Edit Summary of A Organization</legend>
                
                <input 
                           class="form-control"
                           type="hidden"
                           name="id"
                           
                           
                        
                         
                           value="<?php echo $summaries['id'];?>"
                           />  
                
                
               
                <div class="form-group">
                    
                    <label for="organization">Edit Name of the Organization: </label>
                    <input placeholder="Enter name of the organization"
                           class="form-control"
                           type="text"
                           name="organization"
                           id="organization"
                           autofocus="autofocus"
                           tabindex="1"
                           required="required"
                           size="50"
                           value="<?php echo $summaries['organization'];?>"/>
                </div> 
                  
                <div class="form-group">    
                    <label for="email">Edit Summary : </label><br>
                    <textarea class="form-control" name="summary" rows="5" placeholder="Write summary of the organization"
                              tabindex="2"><?php echo $summaries['summary'];?>                       
                    </textarea>        
                </div>
                    
                
                <div class="form-group">
                    <button tabindex="3"type="Submit" class="btn btn-info">Save</button>
                    <button tabindex="4"type="Submit" class="btn btn-info">Save & Add</button>
                    <button tabindex="5"type="reset" class="btn btn-info">Reset</button>
                
                </div>
            </fieldset>    
        </form>
        
            <nav>
                <li><a href="index.php">Go To List</a></li>
                <li><a href="javascript:history.go(-1)">Back</a></li>
            </nav>
        </div>       
    </body>
</html>
