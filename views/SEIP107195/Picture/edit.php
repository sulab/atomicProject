<?php
        ini_set('display_errors','off');
        include_once ("../../../vendor/autoload.php");
        use \Sulab\Bitm\SEIP107095\Picture\ProfilePicture;
        
        
        
        $profile = new ProfilePicture();
        
        $pictures = $profile->show($_GET['id']);
        
        
        ?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>Profile Picture</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="../../../resource/css/bootstrap.min.css">
        <script src="../../../resource/js/jquery.min.js"></script>
        <script src="../../../resource/js/bootstrap.min.js"></script>

        
    </head>
    <body>
       <br><br>
        <div class="container">
            <form role="form" action="update.php" method="post" enctype="multipart/form-data">
            <fieldset class="form-group">
                <legend>Add Profile Picture</legend>
                <input 
                           class="form-control"
                           type="hidden"
                           name="id"                         
                           value="<?php echo $pictures['id'];?>"
                           />  
                
                
                <div class="form-group">
                    <label for="username">Enter Your Name : </label>
                    <input placeholder="Enter your name "
                           class="form-control"
                           type="text"
                           name="username"
                           id="username"
                           autofocus="autofocus"
                           tabindex="1"
                           required="required"
                           size="50"
                           value="<?php echo $pictures['username'];?>"
                           />
                </div>    
                <div class="form-group">    
                    <label for="picture">Select Your Profile Picture : </label>
                    <input 
                           type="file"
                           name="picture"
                           id="picture"
                           autofocus="autofocus"
                           tabindex="2"
                           required="required"
                           value=""
                           />
                    
                </div>
                
                <div class="form-group">
                    <button tabindex="3"type="Submit" class="btn btn-info">Save</button>
                    <button tabindex="4"type="Submit" class="btn btn-info">Save & Add</button>
                    <button tabindex="5"type="reset" class="btn btn-info">Reset</button>
                
                </div>
            </fieldset>    
        </form>
        
            <nav>
                <li><a href="index.php">Go To List</a></li>
                <li><a href="javascript:history.go(-1)">Back</a></li>
            </nav>
        </div>   
  </body>
</html>
