<?php

namespace  Sulab\Bitm\SEIP107095\Academic;

use Sulab\Bitm\SEIP107095\Utility\Utility;
        
class BookTitle {
    public $id = "";
    public $title = "";
    public $author = "";
    //public $created;
    //public $modified;
    //public $created_by;
    //public $modified_by;
    //public $deleted_at;
    
    
    public function __construct($param = false) {
        
        if(is_array($param)&&array_key_exists('id', $param)&& !empty($param['id'])){           
         $this->id = $param['id']; 
        }
        
         $this->title = $param['title'];
         $this->author = $param['author'];
    }

    
    public function index() {
        
        $booktitles = array(); 
        $con = mysql_connect("localhost","root","") or die("cannot connect to database...");
        $link = mysql_select_db("atomicproject_107095") or die("cannot select database...");
        
        $query = "SELECT * FROM `booktitles`";  
        
        $result =  mysql_query($query);
        
        while ($row = mysql_fetch_assoc($result)) {
            $booktitles[] = $row;
        }
        return $booktitles;
        
        
    }
    
    public function show($id = false){
        
        $con = mysql_connect("localhost","root","") or die("cannot connect to database...");
        $link = mysql_select_db("atomicproject_107095") or die("cannot select database...");
        
        $query = "SELECT * FROM `booktitles` WHERE id =".$id;  
        
        $result =  mysql_query($query);
        $row = mysql_fetch_assoc($result);
        
        return $row;
        
    }

        public function create() {
        echo "I create form.";
    }
    
    public function store() {        
        
        $con = mysql_connect("localhost","root","") or die("cannot connect to database...");
        $link = mysql_select_db("atomicproject_107095") or die("cannot select database...");
        
        $query = "INSERT INTO `bookTitles` ( `title`,`author`) VALUES ( '".$this->title."','".$this->author."')";
        $result =  mysql_query($query);
        
        if($result){
            Utility::message("Book Title is added successfully!!!!!");
        } 
        else {
            Utility::message("There is an error. Please try again.");
        }
       
        Utility::redirect_booktitle();
    }
    
    public function edit() {
        echo "I am editing data.";
    }
    
    public function update() {
        
   
        $con = mysql_connect("localhost","root","") or die("cannot connect to database...");
        $link = mysql_select_db("atomicproject_107095") or die("cannot select database...");
        
        $query = "UPDATE `booktitles` SET `title` = '".$this->title."', `author` = '".$this->author."' WHERE `booktitles`.`id` = ".$this->id; 
        $result =  mysql_query($query);
        
        if($result){
            Utility::message("Book Title is  edited successfully!!!!!");
        } 
        else {
            Utility::message("There is an error. Please try again.");
        }
       
        Utility::redirect_booktitle();

    }
    
    public function delete($id = null) {
        
        if(is_null($id)){
            Utility::message("No id is available. Sorry!!!");
            return Utility::redirect_booktitle();
        }
        
        $con = mysql_connect("localhost","root","") or die("cannot connect to database...");
        $link = mysql_select_db("atomicproject_107095") or die("cannot select database...");
        
        $query = "DELETE FROM `booktitles` WHERE `booktitles`.`id` = ".$id;
        
        $result =  mysql_query($query);
        
        if($result){
            Utility::message("Book Title is deleted successfully!!!!!");
        } 
        else {
            Utility::message("Cannot delete...");
        }
       
        Utility::redirect_booktitle();
    }
    
}
?>