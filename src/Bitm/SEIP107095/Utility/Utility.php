<?php

namespace Sulab\Bitm\SEIP107095\Utility; 
session_start();
class Utility {
    
    
    public static $message;


    static public function d($param = false) {
        echo "<pre>";
        var_dump($param);
        echo "<pre>";
    }
    
    static public function dd($param = false) {
        self::d($param);
        die();
    }
    
    static public function redirect_booktitle($url="/atomicProject_107095/views/SEIP107195/Academic/index.php") {
        header("location:".$url);   
    }
     static public function redirect_hobby($url="/atomicProject_107095/views/SEIP107195/Leisure/index.php") {
        header("location:".$url);   
    }
     static public function redirect_email($url="/atomicProject_107095/views/SEIP107195/Subscription/index.php") {
        header("location:".$url);   
    }
    static public function redirect_city($url="/atomicProject_107095/views/SEIP107195/geographicalArea/index.php") {
        header("location:".$url);   
    }
    static public function redirect_summary($url="/atomicProject_107095/views/SEIP107195/Organization/index.php") {
        header("location:".$url);   
    }
    static public function redirect_gender($url="/atomicProject_107095/views/SEIP107195/Gender/index.php") {
        header("location:".$url);   
    }
    static public function redirect_birthday($url="/atomicProject_107095/views/SEIP107195/Date/index.php") {
        header("location:".$url);   
    }
    static public function redirect_picture($url="/atomicProject_107095/views/SEIP107195/Picture/index.php") {
        header("location:".$url);   
    }
    static public function redirect_condition($url="/atomicProject_107095/views/SEIP107195/Conditions/index.php") {
        header("location:".$url);   
    }
    
    
    static public function message($message = null){
        if(is_null($message)){ // please give me message
            $mes = self::getMessage();
            return $mes;
        }else{ //please set this message
            self::setMessage($message);
        }
    }
    
    static private function getMessage(){
        
        $mes =  $_SESSION['message'];
        $_SESSION['message'] = "";
        return $mes;
    }
    
    static private function setMessage($message){
        $_SESSION['message'] = $message;
    }
}
