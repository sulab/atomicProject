<?php

namespace Sulab\Bitm\SEIP107095\Subscription;
use \Sulab\Bitm\SEIP107095\Utility\Utility;
class Email {
 
    public $id = "";
    public $email = "";
    public $username = "";
    public $created;
    public $modified;
    public $created_by;
    public $modified_by;
    public $deleted_at;
    
    
    public function __construct($param = false) {
        if(is_array($param) && array_key_exists('id', $param)&& !empty($param['id'])){           
         $this->id = $param['id']; 
        }
        
        $this->username = $param['username'];
        $this->email = $param['email'];
    }

    
    public function index() {
        $emails = array(); 
        
        $con = mysql_connect("localhost","root","") or die("cannot connect to database...");
        $link = mysql_select_db("atomicproject_107095") or die("cannot select database...");
        
        $query = "SELECT * FROM `emails`";  
        
        $result =  mysql_query($query);
        
        while ($row = mysql_fetch_assoc($result)) {
            $emails[] = $row;
        }
        return $emails;
    }
    
    public function show($id = false) {
        
        $con = mysql_connect("localhost","root","") or die("cannot connect to database...");
        $link = mysql_select_db("atomicproject_107095") or die("cannot select database...");
        
        $query = "SELECT * FROM `emails` WHERE id =".$id;  
        
        $result =  mysql_query($query);
        $row = mysql_fetch_assoc($result);
        
        return $row;
    }
    
    public function store() {
        $con = mysql_connect("localhost", "root", "") or die("cannot connect to database");
        $link = mysql_select_db("atomicproject_107095") or die("cannot select database...");
        
        $query = "INSERT INTO `emails` ( `username`, `email`) VALUES ( '".$this->username."', '".$this->email."')";
        $result = mysql_query($query);
        
      
        if($result){
            Utility::message("E-mail is added successfully!!!!!");
        } 
        else {
            Utility::message("There is an error. Please try again.");
        }
       
        Utility::redirect_email();
    }
    
    public function edit() {
        echo "I am editing data.";
    }
    
    public function update() {
        
        $con = mysql_connect("localhost","root","") or die("cannot connect to database...");
        $link = mysql_select_db("atomicproject_107095") or die("cannot select database...");
        
        $query = "UPDATE `atomicproject_107095`.`emails` SET `username` = '$this->username', `email` = '".$this->email."' WHERE `emails`.`id` = ".$this->id;
        $result =  mysql_query($query);
       
        if($result){
            Utility::message("E-mail is  edited successfully!!!!!");
        } 
        else {
            Utility::message("There is an error. Please try again.");
        }
       
        Utility::redirect_email();
    }
    
    public function delete($id = null) {
        if(is_null($id)){
            Utility::message("No id is available. Sorry!!!");
            return Utility::redirect_email();
        }
        
        $con = mysql_connect("localhost","root","") or die("cannot connect to database...");
        $link = mysql_select_db("atomicproject_107095") or die("cannot select database...");
        
        $query = "DELETE FROM `emails` WHERE `emails`.`id` = ".$id;
        
        $result =  mysql_query($query);
        
        if($result){
            Utility::message("E-mail is deleted successfully!!!!!");
        } 
        else {
            Utility::message("Cannot delete...");
        }
       
        Utility::redirect_email();
    }
}
