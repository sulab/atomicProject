<?php


namespace Sulab\Bitm\SEIP107095\Picture;
use \Sulab\Bitm\SEIP107095\Utility\Utility;

class ProfilePicture {
    public $id = "";
    public $username = ""; 
    public $picture = "";
    public $created;
    public $modified;
    public $created_by;
    public $modified_by;
    public $deleted_at;
    
    
    public function __construct($id = false, $username = false, $picture = false) {
        if(!is_int($id)){           
         $this->id = $id; 
        }
        $this->username = $username;
        $filetmp = $_FILES['picture']["tmp_name"];
        $filename = $_FILES['picture']["name"];
        $filepath = "file/" . $filename;
        $this->picture = $filepath;
        move_uploaded_file($filetmp, $filepath);

         
        
        
    }

    
    public function index() {
        $pictures = array(); 
        
        $con = mysql_connect("localhost","root","") or die("cannot connect to database...");
        $link = mysql_select_db("atomicproject_107095") or die("cannot select database...");
        
        $query = "SELECT * FROM `profile`";  
        
        $result =  mysql_query($query);
        
        while ($row = mysql_fetch_assoc($result)) {
            $pictures[] = $row;
        }
        return $pictures;
    }
    
    
    public function show($id = false) {
        
        $con = mysql_connect("localhost","root","") or die("cannot connect to database...");
        $link = mysql_select_db("atomicproject_107095") or die("cannot select database...");
        
        $query = "SELECT * FROM `profile` WHERE id =".$id;  
        
        $result =  mysql_query($query);
        $row = mysql_fetch_assoc($result);
        
        return $row;
        
    }
    
    public function store() {
        $con = mysql_connect("localhost", "root", "") or die("cannot connect to database");
        $link = mysql_select_db("atomicproject_107095") or die("cannot select database...");
        
        $query = "INSERT INTO `profile` ( `username`, `picture`) VALUES ( '".$this->username."', '".$this->picture."')";
       
        $result = mysql_query($query);
        
        if($result){
            Utility::message("Profile picture is added successfully!!!!!");
        } 
        else {
            Utility::message("There is an error. Please try again.");
        }
       
        
        Utility::redirect_picture();
        
    }
    
    public function edit() {
        echo "I am editing data.";
    }
    
    public function update() {
        
        $con = mysql_connect("localhost","root","") or die("cannot connect to database...");
        $link = mysql_select_db("atomicproject_107095") or die("cannot select database...");
        
        $query = "UPDATE `profile` SET `username` = '".$this->username."', `picture` = '".$this->picture."' WHERE `profile`.`id` = ".$this->id; 
        //var_dump($query);die();
        $result =  mysql_query($query);
        
        if($result){
            Utility::message("Profile picture is  edited successfully!!!!!");
        } 
        else {
            Utility::message("There is an error. Please try again.");
        }
       
        Utility::redirect_picture();

    }
    
    public function delete($id = null) {
        
        if(is_null($id)){
            Utility::message("No id is available. Sorry!!!");
            return Utility::redirect_picture();
        }
        
        $con = mysql_connect("localhost","root","") or die("cannot connect to database...");
        $link = mysql_select_db("atomicproject_107095") or die("cannot select database...");
        
        $query = "DELETE FROM `profile` WHERE `profile`.`id` = ".$id;
        
        
        $result =  mysql_query($query);
        
        if($result){
            Utility::message("Profile picture is deleted successfully!!!!!");
        } 
        else {
            Utility::message("Cannot delete...");
        }
       
        Utility::redirect_picture();
    }
}
