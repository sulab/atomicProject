<?php

namespace Sulab\Bitm\SEIP107095\Organization;
use \Sulab\Bitm\SEIP107095\Utility\Utility;

class Summary {
    public $id ="";
    public $organization ="";
    public $summary ="";
    public $created;
    public $modified;
    public $created_by;
    public $modified_by;
    public $deleted_at;
    
    
    public function __construct($param = false) {
        if(is_array($param) && array_key_exists('id', $param)&& !empty($param['id'])){           
         $this->id = $param['id']; 
        }
        
        $this->organization = $param['organization'];
        $this->summary = $param['summary'];
    }

    
    public function index() {
        $summaries = array(); 
        
        $con = mysql_connect("localhost","root","") or die("cannot connect to database...");
        $link = mysql_select_db("atomicproject_107095") or die("cannot select database...");
        
        $query = "SELECT * FROM `summary`";  
        
        $result =  mysql_query($query);
        
        while ($row = mysql_fetch_assoc($result)) {
            $summaries[] = $row;
        }
        return $summaries;
    }
    
    public function show($id = false) {
        
        $con = mysql_connect("localhost","root","") or die("cannot connect to database...");
        $link = mysql_select_db("atomicproject_107095") or die("cannot select database...");
        
        $query = "SELECT * FROM `summary` WHERE id =".$id;  
        
        $result =  mysql_query($query);
        $row = mysql_fetch_assoc($result);
        
        return $row;
        
    }
    
    public function store() {
        $con = mysql_connect("localhost", "root", "") or die("cannot connect to database");
        $link = mysql_select_db("atomicproject_107095") or die("cannot select database...");
        
        $query = "INSERT INTO `summary` ( `organization`, `summary`) VALUES ( '".$this->organization."', '".$this->summary."')";
        $result = mysql_query($query);
        
        if($result){
            Utility::message("Organization Summary is added successfully!!!!!");
        } 
        else {
            Utility::message("There is an error. Please try again.");
        }
        
        Utility::redirect_summary();
    }
    
    public function edit() {
        echo "I am editing data.";
    }
    
    public function update() {
        
        $con = mysql_connect("localhost","root","") or die("cannot connect to database...");
        $link = mysql_select_db("atomicproject_107095") or die("cannot select database...");
        
        $query = "UPDATE `summary` SET `organization` = '".$this->organization."', `summary` = '".$this->summary."' WHERE `summary`.`id` = ".$this->id;
        $result =  mysql_query($query);
      
        
        if($result){
            Utility::message("Organization Summary is  edited successfully!!!!!");
        } 
        else {
            Utility::message("There is an error. Please try again.");
        }
       
        Utility::redirect_summary();

    }
    
    public function delete($id = null) {
        if(is_null($id)){
            Utility::message("No id is available. Sorry!!!");
            return Utility::redirect_summary();
        }
        
        $con = mysql_connect("localhost","root","") or die("cannot connect to database...");
        $link = mysql_select_db("atomicproject_107095") or die("cannot select database...");
        
        $query = "DELETE FROM `summary` WHERE `summary`.`id` = ".$id;
        
        $result =  mysql_query($query);
        
        if($result){
            
            Utility::message("Organization summary is deleted successfully!!!!!");
        } 
        else {
            Utility::message("Cannot delete...");
        }
       
        Utility::redirect_summary();
    }
}
