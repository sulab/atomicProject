<?php

namespace Sulab\Bitm\SEIP107095\Conditions;
use \Sulab\Bitm\SEIP107095\Utility\Utility;
class Terms {
    public $id = "";
    public $condition = "";
    public $username = "";
    public $created;
    public $modified;
    public $created_by;
    public $modified_by;
    public $deleted_at;
    
    
    public function __construct($param = false) {
        if(is_array($param)&&array_key_exists('id', $param)&& !empty($param['id'])){           
         $this->id = $param['id']; 
        }
        
        $this->condition = $param['condition'];
        $this->username = $param['username'];
    }

    
    public function index() {
        $conditions = array(); 
        
        $con = mysql_connect("localhost","root","") or die("cannot connect to database...");
        $link = mysql_select_db("atomicproject_107095") or die("cannot select database...");
        
        $query = "SELECT * FROM `conditions`";  
        
        $result =  mysql_query($query);
        
        while ($row = mysql_fetch_assoc($result)) {
            $conditions[] = $row;
        }
        return $conditions;
    }
    
    public function show($id = false) {
        
        $con = mysql_connect("localhost","root","") or die("cannot connect to database...");
        $link = mysql_select_db("atomicproject_107095") or die("cannot select database...");
        
        $query = "SELECT * FROM `conditions` WHERE id =".$id;  
        
        $result =  mysql_query($query);
        $row = mysql_fetch_assoc($result);
        
        return $row;
        
    }
    
    public function store() {

        $con = mysql_connect("localhost", "root", "") or die("cannot connect to database");
        $link = mysql_select_db("atomicproject_107095") or die("cannot select database...");
        
        $query = "INSERT INTO `conditions` ( `username`, `criteria`) VALUES ( '".$this->username."', '".$this->condition."')";
        $result = mysql_query($query);
        
        if($result){
            Utility::message("Your choice of condition is added successfully!!!!!");
        } 
        else {
            Utility::message("There is an error. Please try again.");
        }
       
        

        Utility::redirect_condition();        
        
    }
    
    public function edit() {
        echo "I am editing data.";
    }
    
    public function update() {
        
        $con = mysql_connect("localhost","root","") or die("cannot connect to database...");
        $link = mysql_select_db("atomicproject_107095") or die("cannot select database...");
        
        $query = "UPDATE `conditions` SET `username` = '".$this->username."', `criteria` = '".$this->condition."' WHERE `conditions`.`id` = ".$this->id;
        $result =  mysql_query($query);
        
        if($result){
            Utility::message("Your choice of condition is edited successfully!!!!!");
        } 
        else {
            Utility::message("There is an error. Please try again.");
        }
        
        Utility::redirect_condition();
       
        
    }
    
    public function delete($id = null) {
        
        if(is_null($id)){
            Utility::message("No id is available. Sorry!!!");
            return Utility::redirect_condition();
        }
        
        $con = mysql_connect("localhost","root","") or die("cannot connect to database...");
        $link = mysql_select_db("atomicproject_107095") or die("cannot select database...");
        
        $query = "DELETE FROM `conditions` WHERE `conditions`.`id` = ".$id;
        
        $result =  mysql_query($query);
        
        if($result){
            Utility::message("Your choice of condition is deleted successfully!!!!!");
        } 
        else {
            Utility::message("Cannot delete...");
        }
        
        Utility::redirect_condition();
       
    }
}
