<?php

namespace Sulab\Bitm\SEIP107095\Leisure;
use \Sulab\Bitm\SEIP107095\Utility\Utility;

class Hobby {
    public $id = "";
    public $username = "";
    public $hobby = "";
    public $created;
    public $modified;
    public $created_by;
    public $modified_by;
    public $deleted_at;
    
    
    public function __construct($param= false) {
        if(is_array($param) && array_key_exists('id', $param)&& !empty($param['id'])){           
         $this->id = $param['id']; 
        }
        $this->username = $param['username'];
        $this->hobby = implode(",", $param['hobby']);
    }

   
    public function index() {
        $hobbies = array(); 
        
        $con = mysql_connect("localhost","root","") or die("cannot connect to database...");
        $link = mysql_select_db("atomicproject_107095") or die("cannot select database...");
        
        $query = "SELECT * FROM `hobbies`";  
        
        $result =  mysql_query($query);
        
        while ($row = mysql_fetch_assoc($result)) {
            $hobbies[] = $row;
        }
        return $hobbies;
    }
    
    public function show($id = false) {
        
        $con = mysql_connect("localhost","root","") or die("cannot connect to database...");
        $link = mysql_select_db("atomicproject_107095") or die("cannot select database...");
        
        $query = "SELECT * FROM `hobbies` WHERE id =".$id;  
        
        $result =  mysql_query($query);
        $row = mysql_fetch_assoc($result);
        
        return $row;
        
    }
    
    public function store() {
        $con = mysql_connect("localhost", "root", "") or die("cannot connect to database");
        $link = mysql_select_db("atomicproject_107095") or die("cannot select database...");
        
        $query = "INSERT INTO `hobbies` ( `username`, `hobby`) VALUES ( '".$this->username."', '".$this->hobby."')";
        $result = mysql_query($query);
        
        if($result){
            Utility::message("Your hobby is added successfully!!!!!");
        } 
        else {
            Utility::message("There is an error. Please try again.");
        }
       
        
        Utility::redirect_hobby();
        
    }
    
    public function edit() {
        echo "I am editing data.";
    }
    
    public function update() {
        
        $con = mysql_connect("localhost","root","") or die("cannot connect to database...");
        $link = mysql_select_db("atomicproject_107095") or die("cannot select database...");
        
        $query = "UPDATE `hobbies` SET `username` = '".$this->username."', `hobby` = '".$this->hobby."' WHERE `hobbies`.`id` = ".$this->id; 
        $result =  mysql_query($query);
        
        if($result){
            Utility::message("Your hobby is  edited successfully!!!!!");
        } 
        else {
            Utility::message("There is an error. Please try again.");
        }
        Utility::redirect_hobby();
    }
    
    public function delete($id = null) {
        if(is_null($id)){
            Utility::message("No id is available. Sorry!!!");
            return Utility::redirect_hobby();
        }
        
        $con = mysql_connect("localhost","root","") or die("cannot connect to database...");
        $link = mysql_select_db("atomicproject_107095") or die("cannot select database...");
        
        $query = "DELETE FROM `hobbies` WHERE `hobbies`.`id` = ".$id;
        
        $result =  mysql_query($query);
        
        if($result){
            Utility::message("Hobby is deleted successfully!!!!!");
        } 
        else {
            Utility::message("Cannot delete...");
        }
        
        Utility::redirect_hobby();
       
    }
}
