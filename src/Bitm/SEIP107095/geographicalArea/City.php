<?php

namespace Sulab\Bitm\SEIP107095\geographicalArea;
use \Sulab\Bitm\SEIP107095\Utility\Utility;

class City {
    public $id;
    public $cityname = "";
    public $username = "";
    public $created;
    public $modified;
    public $created_by;
    public $modified_by;
    public $deleted_at;
    
    
    public function __construct($param = false) {
        
        if(is_array($param)&&array_key_exists('id', $param)&& !empty($param['id'])){           
                    $this->id = $param['id']; 
        }
        
        $this->username = $param['username'];
        $this->cityname = $param['cityname'];
    }

    
    public function index() {
        $citynames = array(); 
        
        $con = mysql_connect("localhost","root","") or die("cannot connect to database...");
        $link = mysql_select_db("atomicproject_107095") or die("cannot select database...");
        
        $query = "SELECT * FROM `citynames`";  
        
        $result =  mysql_query($query);
        
        while ($row = mysql_fetch_assoc($result)) {
            $citynames[] = $row;
        }
        return $citynames;
        
        
    }
    
    public function show($id = false) {
        
        $con = mysql_connect("localhost","root","") or die("cannot connect to database...");
        $link = mysql_select_db("atomicproject_107095") or die("cannot select database...");
        
        $query = "SELECT * FROM `citynames` WHERE id =".$id;  
        
        $result =  mysql_query($query);
        $row = mysql_fetch_assoc($result);
        
        return $row;
        
    }
    
    public function store() {
        
        $con = mysql_connect("localhost", "root", "") or die("cannot connect to database");
        $link = mysql_select_db("atomicproject_107095") or die("cannot select database...");
        
        $query = "INSERT INTO `citynames` ( `username`, `cityname`) VALUES ( '".$this->username."', '".$this->cityname."')";
        $result = mysql_query($query);
        
        Utility::redirect_city();
       
        if($result){
            Utility::message("Book Title is added successfully!!!!!");
        } 
        else {
            Utility::message("There is an error. Please try again.");
        }
       
        Utility::redirect_city();
    }
    
    public function edit() {
        echo "I am editing data.";
    }
    
    public function update() {
        
        $con = mysql_connect("localhost","root","") or die("cannot connect to database...");
        $link = mysql_select_db("atomicproject_107095") or die("cannot select database...");
        
        $query = "UPDATE `citynames` SET `username` = '".$this->username."', `cityname` = '".$this->cityname."' WHERE `citynames`.`id` = ".$this->id; 
        $result =  mysql_query($query);
        
        if($result){
            Utility::message("City Name is  edited successfully!!!!!");
        } 
        else {
            Utility::message("There is an error. Please try again.");
        }
       
        Utility::redirect_city();

    }
    
    public function delete($id = null) {
        
        if(is_null($id)){
            Utility::message("No id is available. Sorry!!!");
            return Utility::redirect_city();
        }
        
        $con = mysql_connect("localhost","root","") or die("cannot connect to database...");
        $link = mysql_select_db("atomicproject_107095") or die("cannot select database...");
        
        $query = "DELETE FROM `citynames` WHERE `citynames`.`id` = ".$id;
        
        $result =  mysql_query($query);
        
        
        if($result){
            Utility::message("City name is deleted successfully!!!!!");
        } 
        else {
            Utility::message("Cannot delete...");
        }
       
        Utility::redirect_city();
    }
}
