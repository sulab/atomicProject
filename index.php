       

<html>
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Home</title>
        <link rel="stylesheet" href="resource/css/bootstrap.min.css">
        <script src="resource/js/jquery.min.js"></script>
        <script src="resource/js/bootstrap.min.js"></script>
        <style>
            a{text-decoration: none;}
            h1,h2,h3{color: green;}
            .container {width: 60%;}
            body{
                background-color: #C5EFF7;
            }
        </style>
    </head>
    <body>
        
        <div class="header">
                <div class="container text-center">
                    <div class="row">
                        <h1>BITM-Web Application Development-PHP</h1>
                    </div>
                </div>    
        </div>
        <div class="information">
        <div class="container text-center">
                    <div class="row">
                        <div class="col-md-4">
                            
                        </div>
                        <div class="col-md-4">
                            <dl>
                                <dt>Name:</dt>
                                <dd>Sulab Saha</dd> 
            
                                <dt>SEIP ID:</dt>
                                <dd>107095</dd>
            
                                <dt>Batch:</dt>
                                <dd>10</dd>
                            </dl>
                        </div>
                        <div class="col-md-4">
                            
                        </div>
                    </div>
        
        
        
        <h2>Projects</h2>
        </div>
        </div>    
        
        <div class="container">                    
        <table class="table table-bordered text-center">
            <thead>
                <tr>
                    <th style="text-align: center;">Serial No.</th>
                    <th style="text-align: center;">Project Name</th>
                </tr>
            </thead>
            
            <tbody>
                <tr>
                    <td>01</td>
                    <td><a href="views/SEIP107195/Academic/index.php">Book Title</a></td>
                </tr>
                <tr>
                    <td>02</td>
                    <td><a href="views/SEIP107195/geographicalArea/index.php">City Name</a></td>
                </tr>
                <tr>
                    <td>03</td>
                    <td><a href="views/SEIP107195/Subscription/index.php">E-mail Subscription</a></td>
                </tr>
                <tr>
                    <td>04</td>
                    <td><a href="views/SEIP107195/Organization/index.php">Summary of Organizations</a></td>
                </tr>
                <tr>
                    <td>05</td>
                    <td><a href="views/SEIP107195/Leisure/index.php">Hobby</a></td>
                </tr>
                <tr>
                    <td>06</td>
                    <td><a href="views/SEIP107195/Gender/index.php">Gender</a></td>
                </tr>
                <tr>
                    <td>07</td>
                    <td><a href="views/SEIP107195/Date/index.php">Birthday</a></td>
                </tr>
                <tr>
                    <td>08</td>
                    <td><a href="views/SEIP107195/Picture/index.php">Profile Picture</a></td>
                </tr>
                <tr>
                    <td>09</td>
                    <td><a href="views/SEIP107195/Conditions/index.php">Terms & Conditions</a></td>
                </tr> 






                
            </tbody>
        </table>
        </div>
           
       
        
    </body>
</html>
